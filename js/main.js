const tails = document.getElementById("tails");
const sonic = document.getElementById("sonic");

const tailsRock = document.getElementById("tailsRock");
const tailsPaper = document.getElementById("tailsPaper");
const tailsScissors = document.getElementById("tailsScissors");

const sonicRock = document.getElementById("sonicRock");
const sonicPaper = document.getElementById("sonicPaper");
const sonicScissors = document.getElementById("sonicScissors");

let sonicSelected = 0;
let tailsSelected = 0;

const sonicChooseRock = 1;
const sonicChoosePaper = 2;
const sonicChooseScissors = 3;

const tailsStats = document.getElementById("tailsStats");
const sonicStats = document.getElementById("sonicStats")


let win = document.getElementById("sound2");
let lose = document.getElementById("sound3");
let draw = document.getElementById("sound4");

function winSound() {
    win.play();
}

function loseSound() {
    lose.play();
}

function drawSound() {
    draw.play()
}




//-------------POSIÇÕES E IMAGENS INICIAIS-------------------//

function beginGame() {
    tails.innerHTML = '<img src="img/tails_normal.png" alt="">';
    sonic.innerHTML = '<img src="img/sonic_normal.png" alt="">';

    tailsRock.innerHTML = '<img src="img/stone.png" alt="" height="90px" width="90px">';
    tailsPaper.innerHTML = '<img src="img/paper.png" alt="" height="90px" width="90px">';
    tailsScissors.innerHTML = '<img src="img/scissors.png" alt="" height="90px" width="90px">';

    sonicRock.innerHTML = '<img src="img/stone.png" alt="" height="90px" width="90px">';
    sonicPaper.innerHTML = '<img src="img/paper.png" alt="" height="90px" width="90px">';
    sonicScissors.innerHTML = '<img src="img/scissors.png" alt="" height="90px" width="90px">';
}

beginGame()

//-------------LISTENERS-------------------//

tailsRock.addEventListener('click', chooseRock);
tailsPaper.addEventListener('click', choosePaper);
tailsScissors.addEventListener('click', chooseScissors);

//-------------FUNCTIONS----------------------------------------------------------//
                                                                                  //
                                                                                  //


//---------TAILS ESCOLHEU PEDRA------------//

function chooseRock() {
    tailsRock.style.animation = 'selectedCard 0.6s ease-in-out both'; 
    tailsPaper.style.animation = 'fade-out 1s ease-out 0.2s both';
    tailsScissors.style.animation = 'fade-out 1s ease-out 0.4s both';

    sonicSelected = Math.ceil(Math.random() * 3);

    if (sonicSelected === sonicChooseRock) {
        sonicRock.style.animation = 'fade-in 1s ease-out both';
        sonicPaper.style.animation = 'fade-out 1s ease-out 0.6s both';
        sonicScissors.style.animation = 'fade-out 1s ease-out 0.6s both';
        tails.innerHTML = '<img src="img/tails_draw.png" alt="">';
        sonic.innerHTML = '<img src="img/sonic_draw.png" alt="">';
        tailsStats.innerHTML = 'DRAW';
        sonicStats.innerHTML = 'DRAW';
        draw.play()
    }

    if (sonicSelected === sonicChoosePaper) {
        sonicRock.style.animation = 'fade-out 1s ease-out 0.6s both';
        sonicPaper.style.animation = 'fade-in 1s ease-out both';
        sonicScissors.style.animation = 'fade-out 1s ease-out 0.6s both';
        tails.innerHTML = '<img src="img/tails_lose.png" alt="">';
        sonic.innerHTML = '<img src="img/sonic_win.png" alt="">';
        tailsStats.innerHTML = 'LOSE';
        sonicStats.innerHTML = 'WIN';
        lose.play()
    }

    if (sonicSelected === sonicChooseScissors) {
        sonicRock.style.animation = 'fade-out 1s ease-out 0.6s both';
        sonicPaper.style.animation = 'fade-out 1s ease-out 0.6s both';
        sonicScissors.style.animation = 'fade-in 1s ease-out both';
        tails.innerHTML = '<img src="img/tails_win.png" alt="">';
        sonic.innerHTML = '<img src="img/sonic_lose.png" alt="">';
        tailsStats.innerHTML = 'WIN';
        sonicStats.innerHTML = 'LOSE';
        win.play()
    }
}

//---------TAILS ESCOLHEU PAPEL-------------//
function choosePaper() {
    tailsPaper.style.animation = 'selectedCard 0.6s ease-in-out both'; 
    tailsRock.style.animation = 'fade-out 1s ease-out 0.2s both';
    tailsScissors.style.animation = 'fade-out 1s ease-out 0.6s both';

    sonicSelected = Math.ceil(Math.random() * 3);

    if (sonicSelected === sonicChooseRock) {
        sonicRock.style.animation = 'fade-in 1s ease-out both';
        sonicPaper.style.animation = 'fade-out 1s ease-out 0.6s both';
        sonicScissors.style.animation = 'fade-out 1s ease-out 0.6s both';
        tails.innerHTML = '<img src="img/tails_win.png" alt="">';
        sonic.innerHTML = '<img src="img/sonic_lose.png" alt="">';
        tailsStats.innerHTML = 'WIN';
        sonicStats.innerHTML = 'LOSE';
        win.play()
    }

    if (sonicSelected === sonicChoosePaper) {
        sonicRock.style.animation = 'fade-out 1s ease-out 0.6s both';
        sonicPaper.style.animation = 'fade-in 1s ease-out both';
        sonicScissors.style.animation = 'fade-out 1s ease-out 0.6s both';
        tails.innerHTML = '<img src="img/tails_draw.png" alt="">';
        sonic.innerHTML = '<img src="img/sonic_draw.png" alt="">';
        tailsStats.innerHTML = 'DRAW';
        sonicStats.innerHTML = 'DRAW';
        draw.play()
    }

    if (sonicSelected === sonicChooseScissors) {
        sonicRock.style.animation = 'fade-out 1s ease-out 0.6s both';
        sonicPaper.style.animation = 'fade-out 1s ease-out 0.6s both';
        sonicScissors.style.animation = 'fade-in 1s ease-out both';
        tails.innerHTML = '<img src="img/tails_lose.png" alt="">';
        sonic.innerHTML = '<img src="img/sonic_win.png" alt="">';
        tailsStats.innerHTML = 'LOSE';
        sonicStats.innerHTML = 'WIN';
        lose.play()
    }
}

//-----------TAILS ESCOLHEU TESOURA-----------------//
function chooseScissors() {
    tailsScissors.style.animation = 'selectedCard 0.6s ease-in-out both'; 
    tailsPaper.style.animation = 'fade-out 1s ease-out 0.2s both';
    tailsRock.style.animation = 'fade-out 1s ease-out 0.6s both';

    sonicSelected = Math.ceil(Math.random() * 3);

    if (sonicSelected === sonicChooseRock) {
        sonicRock.style.animation = 'fade-in 1s ease-out both';
        sonicPaper.style.animation = 'fade-out 1s ease-out 0.6s both';
        sonicScissors.style.animation = 'fade-out 1s ease-out 0.6s both';
        tails.innerHTML = '<img src="img/tails_lose.png" alt="">';
        sonic.innerHTML = '<img src="img/sonic_win.png" alt="">';
        tailsStats.innerHTML = 'LOSE';
        sonicStats.innerHTML = 'WIN';
        lose.play()
    }

    if (sonicSelected === sonicChoosePaper) {
        sonicRock.style.animation = 'fade-out 1s ease-out 0.6s both';
        sonicPaper.style.animation = 'fade-in 1s ease-out both';
        sonicScissors.style.animation = 'fade-out 1s ease-out 0.6s both';
        tails.innerHTML = '<img src="img/tails_win.png" alt="">';
        sonic.innerHTML = '<img src="img/sonic_lose.png" alt="">';
        tailsStats.innerHTML = 'WIN';
        sonicStats.innerHTML = 'LOSE';
        win.play()
    }

    if (sonicSelected === sonicChooseScissors) {
        sonicRock.style.animation = 'fade-out 1s ease-out 0.6s both';
        sonicPaper.style.animation = 'fade-out 1s ease-out 0.6s both';
        sonicScissors.style.animation = 'fade-in 1s ease-out both';
        tails.innerHTML = '<img src="img/tails_draw.png" alt="">';
        sonic.innerHTML = '<img src="img/sonic_draw.png" alt="">';
        tailsStats.innerHTML = 'DRAW';
        sonicStats.innerHTML = 'DRAW';
        draw.play()
    }
}
